//
//  ArtistDetailView.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 22/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import SwiftUI
import CoreData
import SDWebImageSwiftUI


struct ArtistDetailView: View {
    
//    @EnvironmentObject var favourites: Favourites
    
//    var artist: Artist
    
    @ObservedObject var viewModel: ArtistDetailViewModel
    
    
    var body: some View {
        VStack {
            WebImage(url: viewModel.artist.fullImageUrl)
                .resizable()
                .indicator(.activity)
                .scaledToFit()
                
            Text(viewModel.artist.name)
            Text(viewModel.artist.bio)
            Spacer()
            Text("Performances Count: \(viewModel.performances.count)")
            List(viewModel.performances, id: \.id) { performance in
                Text(performance.name)
            }
            Spacer()
            
            Button(viewModel.buttonText) {
                self.viewModel.buttonTapped()
            }
        .padding()
        .background(Color.green)
        .foregroundColor(.white)
        .cornerRadius(16)
        .clipped()
        }        
    }
    
}

//struct ArtistDetailView_Previews: PreviewProvider {
//    static let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//    static var previews: some View {
//        NavigationView {
//            ArtistDetailView(viewModel: ExampleArtist().artistDetailVM)
//        }
//    }
//}
