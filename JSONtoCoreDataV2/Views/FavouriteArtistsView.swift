//
//  FavouriteArtistsView.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 08/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct FavouriteArtistsView: View {
    
    @ObservedObject var viewModel = FavouriteArtistsViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.artists, id: \.id) { artist in
               NavigationLink(destination: ArtistDetailView(viewModel: ArtistDetailViewModel(for: artist))) {
                   HStack(alignment: .top) {
                       WebImage(url: artist.fullImageUrl)
                       .resizable()
                       .transition(.fade(duration: 0.5))
                       .scaledToFill()
                       .frame(width: 64, height: 64, alignment: .center)
                       .clipped()
                       
                       Text(artist.name)
                    }
                }
            
            .navigationBarTitle(Text("Favourites"))
           }
            .onAppear {
                // when the view loads, refresh the list of favourite artists
                // note: I think this should change to be more 'reactive' the view model shouuld notify the view that smething has changed not just fetch anyway
//                self.viewModel.fetchFavourites()
            }
        }
    }
}

struct FavouriteArtistsView_Previews: PreviewProvider {
    static var previews: some View {
        FavouriteArtistsView()
    }
}
