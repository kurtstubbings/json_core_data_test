//
//  AppView.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 08/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import SwiftUI

struct AppView: View {
    
    var body: some View {
        TabView {
            ArtistListView()
                .tabItem {
                    Image(systemName: "person.3")
                    Text("Line Up")
            }
            FavouriteArtistsView()
                .tabItem {
                    Image(systemName: "heart.fill")
                    Text("Favourites")
            }
        }
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
