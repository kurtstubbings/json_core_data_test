//
//  ContentView.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 19/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI


struct ArtistListView: View {
    
    @ObservedObject private var artistListVM = ArtistListViewModel()
    
    @ObservedObject var favourites = Favourites.shared
    
    var body: some View {
         NavigationView {
            
            List(artistListVM.artists, id: \.id) { artist in
                NavigationLink(destination: ArtistDetailView(viewModel: ArtistDetailViewModel(for: artist))) {
                    HStack(alignment: .top) {
                        WebImage(url: artist.fullImageUrl)
                        .resizable()
                        .transition(.fade(duration: 0.5))
                        .scaledToFill()
                        .frame(width: 64, height: 64, alignment: .center)
                        .clipped()
                        
                        Text(artist.name)
                     }
                 }
                
                // can fix layout of heart
                .layoutPriority(1)
                
                Spacer()
                
                if self.favourites.contains(artist) {

                    Image(systemName: "heart.fill")
                        .foregroundColor(.red)
                } else {
                    Image(systemName: "heart")
                        .foregroundColor(.red)
                }
             }
             .navigationBarTitle(Text("Line Up"))
         }
//        .environmentObject(favourites)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ArtistListView()
    }
}
