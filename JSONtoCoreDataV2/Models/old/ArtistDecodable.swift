//
//  ArtistDecodable.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 19/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

/*
 NO LONGER IN USE
 */

import Foundation
import CoreData

/* reflects Artist type in a nice and easy decodable format */

struct ArtistDecodable: Decodable {
    var id: Int
    var name: String
    var bio: String
    var updated_at: String
    var image: ArtistImage
}
