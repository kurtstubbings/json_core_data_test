//
//  Image.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 22/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

/*
 NO LONGER IN USE
 */

import Foundation

struct ArtistImage: Decodable {
    
    var url: String
    
}
