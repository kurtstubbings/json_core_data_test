//
//  Favourites.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 02/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import SwiftUI

class Favourites: ObservableObject {
    
    static let shared = Favourites()
    
//    var artistIds = [Int]()
    
    @Published var artists: Set<Int32>
    
    private let saveKey = "Favourite Artists"
    
    init() {
        let artistsArray = UserDefaults.standard.array(forKey: saveKey) as? [Int32] ?? []
        
        self.artists = Set(artistsArray)
    }
    
    func all() -> [Int?] {
        let ids = artists.map {
            return Int(exactly: $0)
        }
        return ids
    }
    
    func contains(_ artist: Artist) -> Bool {
        artists.contains(artist.id)
    }
    
    func add(_ artist: Artist) {
        objectWillChange.send()
        artists.insert(artist.id)
        save()
    }
    
    func remove(_ artist: Artist) {
        objectWillChange.send()
        artists.remove(artist.id)
        save()
    }
    
    func save() {
        UserDefaults.standard.set(Array(self.artists), forKey: saveKey)
    }
}
