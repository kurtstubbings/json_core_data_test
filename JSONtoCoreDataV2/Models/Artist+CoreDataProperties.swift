//
//  Artist+CoreDataProperties.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 29/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//
//

import Foundation
import CoreData


extension Artist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Artist> {
        return NSFetchRequest<Artist>(entityName: "Artist")
    }

    // If regenerating this file, remember to make certain things non-optional
    @NSManaged public var bio: String
    @NSManaged public var id: Int32
    @NSManaged public var imageUrl: String
    @NSManaged public var name: String
    @NSManaged public var createdAt: Date
    @NSManaged public var updatedAt: Date
    @NSManaged public var performances: NSSet?

}

// MARK: Generated accessors for performances
extension Artist {

    @objc(addPerformancesObject:)
    @NSManaged public func addToPerformances(_ value: Performance)

    @objc(removePerformancesObject:)
    @NSManaged public func removeFromPerformances(_ value: Performance)

    @objc(addPerformances:)
    @NSManaged public func addToPerformances(_ values: NSSet)

    @objc(removePerformances:)
    @NSManaged public func removeFromPerformances(_ values: NSSet)

}

//MARK: - Computed Properties for niceness

extension Artist {
    // Computed property
    var fullImageUrl: URL {
        let fallbackUrl = URL(string: "https://placehold.it/500x500")!
        return URL(string: "http://localhost:1337")?.appendingPathComponent(imageUrl) ?? fallbackUrl
//        guard let uri = imageUrl else { return fallbackUrl }
//        return URL(string: "http://localhost:1337")?.appendingPathComponent(uri) ?? fallbackUrl
    }
}
