//
//  Performance+CoreDataProperties.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 29/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//
//

import Foundation
import CoreData


extension Performance {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Performance> {
        return NSFetchRequest<Performance>(entityName: "Performance")
    }

    @NSManaged public var endDateTime: Date
    @NSManaged public var id: Int32
    @NSManaged public var startDateTime: Date
    @NSManaged public var name: String
    @NSManaged public var artist: Artist

}
