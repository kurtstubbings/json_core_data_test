//
//  FavouriteArtistsViewModel.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 08/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import Foundation

class FavouriteArtistsViewModel: ObservableObject {
    
    @Published var artists = [Artist]()
        
//    var favourites = Favourites.shared
    
    init() {
        fetchFavourites()
        Favourites.shared.artists.publisher.sink { _ in
            self.fetchFavourites()
        }
    }
    
    func fetchFavourites() {
        artists = CoreDataController.getFavouriteArtists()
        
    }
}

