//
//  ArtistListViewModel.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 19/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//


/*
 - get stages first
 - save stages in array
 - on completion get artists
 */


import Foundation
import CoreData
import SwiftyJSON

class ArtistListViewModel: ObservableObject {
    
    @Published var artists = [Artist]()
    @Published var favouriteImageName = "heart"
//        @Published var artists = [ArtistViewModel]()
    
    let webService = Webservice()

    var favourites = Favourites.shared
    
    let managedObjectContext = CoreDataController.shared.getContext()
    
    init() {
        // populate artists on load, will get updated in fetch (what happens if no artists???)
        artists = CoreDataController.getAllArtists()
        fetchArtists()
    }
    
    func isFavourite(_ artist: Artist) {
        if favourites.contains(artist) {
            favouriteImageName = "heart.fill"
        } else {
            favouriteImageName = "heart"
        }
    }
    
    func configure(_ artist: Artist, usingJSON json: JSON) {
        artist.id = json["id"].int32Value
        artist.name = json["name"].stringValue
        artist.bio = json["bio"].stringValue
        artist.imageUrl = json["image"]["url"].stringValue

        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFractionalSeconds, .withInternetDateTime]
        
        artist.createdAt = formatter.date(from: json["created_at"].stringValue) ?? Date()
        artist.updatedAt = formatter.date(from: json["updated_at"].stringValue) ?? Date()
        
    }
    
    
    func fetchArtists() {
        
        webService.getData(completion: {[weak self] result in
            switch result {
            case .success( let artistsJson ):
                DispatchQueue.main.async {
                    CoreDataController.shared.clearStorage(forEntity: "Artist")
                    
                    for json in artistsJson {
                        let artist = Artist(context: self!.managedObjectContext)
                        self?.configure(artist, usingJSON: json)
                        
                        let performancesJson = json["performances"].arrayValue
                        
                        for json in performancesJson {
                            let performance = Performance(context: self!.managedObjectContext)
                            
                            performance.id = json["id"].int32Value
                            performance.name = json["name"].stringValue
                            
                            let formatter = ISO8601DateFormatter()
                            formatter.formatOptions = [.withFractionalSeconds, .withInternetDateTime]
                            
                            performance.startDateTime = formatter.date(from: json["startDateTime"].stringValue) ?? Date()
                            performance.endDateTime = formatter.date(from: json["endDateTime"].stringValue) ?? Date()
                            
//                            let artistId = json["artist"].int32Value
                            
//                            performance.artist = CoreDataController.getArtist(with: artistId)
                            performance.artist = artist
                            
                            artist.addToPerformances(performance)
                            print("added performance: \(json["name"].stringValue)")
                            
                        }
                        
                        
                    }
                    
                    CoreDataController.shared.saveContext()
                    self?.artists = CoreDataController.getAllArtists()
                    print("artists fetched and saved to core data")


                }
            case .failure(let error):
                print("FetchArtists Failed")
                print(error)
            }
        })
    }
        
    /*
     OLD METHOD
     */
//        func fetchArtists() {
//        print("fetching artists")
//        webService.getData(decodeTo: [ArtistDecodable].self, completion: {[weak self] result in
//            switch result {
//            case .success( let artists ):
//                DispatchQueue.main.async {
//                    CoreDataController.shared.clearStorage(forEntity: "Artist")
//                    
//                    self!.addArtistsToCoreData(artists)
//
//                    CoreDataController.shared.saveContext()
//
//                    // populate the artists array from Core Data
////                    self?.artists = CoreDataController.getAllArtists().map(ArtistViewModel.init)
//                    self?.artists = CoreDataController.getAllArtists()
//                    print("artists fetched and saved to core data")
//                }
//            case .failure(let error):
//                print("FetchArtists Failed")
//                print(error)
//            }
//        })
//    }
    
//    func addArtistsToCoreData(_ artists: [ArtistDecodable]) {
//        for artist in artists {
//            let entity = NSEntityDescription.entity(forEntityName: "Artist", in:  managedObjectContext)
//            let newArtist = NSManagedObject(entity: entity!, insertInto: managedObjectContext)
//
//            newArtist.setValue(artist.id, forKey: "id")
//            newArtist.setValue(artist.name, forKey: "name")
//            newArtist.setValue(artist.bio, forKey: "bio")
//            newArtist.setValue(artist.image.url, forKey: "imageUrl")
//        }
//        print("artists Added")
//    }
}
