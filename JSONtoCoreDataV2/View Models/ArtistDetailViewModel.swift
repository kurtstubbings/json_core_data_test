//
//  ArtistDetailViewModel.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 07/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import Foundation

class ArtistDetailViewModel: ObservableObject {
    var artist: Artist
    
    @Published var performances : [Performance]
    
    var favourites = Favourites.shared
    
//    @Published var buttonText: String {
//        return favourites.contains(artist) ? "Remove from Favourites" : "Add to Favourites"
//    }
    
    @Published var buttonText: String
    
    init(for artist: Artist) {
        self.artist = artist
        
        self.performances = CoreDataController.getPerformances(for: artist)
        
        buttonText = favourites.contains(artist) ? "Remove" : "Add"
    }
    
    
    
    func buttonTapped() {
        if favourites.contains(self.artist) {
            favourites.remove(self.artist)
            buttonText = "Add"
        } else {
            favourites.add(self.artist)
            buttonText = "Remove"
        }
    }
    
    
}
