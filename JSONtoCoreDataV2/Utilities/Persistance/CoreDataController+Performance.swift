//
//  CoreDataController+Performance.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 29/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import Foundation
import CoreData

extension CoreDataController {
    class func getPerformances(for artist: Artist) -> [Performance] {
        let request = NSFetchRequest<Performance>(entityName: "Performance")
        request.predicate = NSPredicate(format: "artist = %@", artist)
        
        var performances = [Performance]()
        
        do {
            let fetched = try CoreDataController.shared.getContext().fetch(request)
            print("fetched \(fetched.count) Performances")
            performances = fetched
        }
        catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        
        return performances
    }
}
