//
//  CoreDataController+Artist.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 29/07/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import Foundation
import CoreData

extension CoreDataController {
    
    class func getAllArtists() -> Array<Artist> {
        let all = NSFetchRequest<Artist>(entityName: "Artist")
        var artists = [Artist]()

        do {
            let fetched = try CoreDataController.shared.getContext().fetch(all)
            artists = fetched
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }

        return artists
    }
    
    class func getFavouriteArtists() -> Array<Artist> {
        let request = NSFetchRequest<Artist>(entityName: "Artist")
        request.predicate = NSPredicate(format: "id IN %@", Favourites.shared.all())
        
        var favourites = [Artist]()
        
        do {
            let fetched = try CoreDataController.shared.getContext().fetch(request)
            favourites = fetched
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        return favourites
    }
    
    class func getArtist(with id: Int32) -> Artist {
        let request = NSFetchRequest<Artist>(entityName: "Artist")
        request.predicate = NSPredicate(format: "id == %@")
        
        var artist = Artist()
        
        do {
            let fetched = try CoreDataController.shared.getContext().fetch(request)
            artist = fetched[0]
        }
        catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        
        return artist
    }
}
