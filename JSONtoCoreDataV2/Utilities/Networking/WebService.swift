//
//  WebService.swift
//  JSONtoCoreDataV2
//
//  Created by Kurt Stubbings on 19/06/2020.
//  Copyright © 2020 Kurt Stubbings. All rights reserved.
//

import Foundation
import SwiftyJSON

enum NetworkError: Error {
    case emptyData
    case decodingError
    case invalidUrl
    case otherError
    case invalidType
}

class Webservice {
    func getData(completion: @escaping (Result<[JSON], NetworkError>) -> Void) {
        
        guard let url = URL(string: "http://localhost:1337/artists") else {
            completion(.failure(.invalidUrl))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if error != nil {
                print(error!.localizedDescription)
                completion(.failure(.otherError))
                return
            }
            
            guard let data = data else {
                completion(.failure(.emptyData))
                return
            }
            
            do {
                let json = try JSON(data: data)
                completion(.success(json.arrayValue))
                
            } catch {
                print (error.localizedDescription)
                completion(.failure(.decodingError))
            }
        }.resume()
    }
    
    
    /*
     Old getData method
     */
    
//    func getData<T: Decodable>(decodeTo: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void) {
//
//        guard let url = URL(string: "http://localhost:1337/artists") else {
//            completion(.failure(.invalidUrl))
//            return
//        }
//
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            if error != nil {
//                completion(.failure(.otherError))
//                return
//            }
//
//            guard let data = data else {
//                completion(.failure(.emptyData))
//                return
//            }
//
//            let decoder = JSONDecoder()
//            do {
//                let decodedData = try decoder.decode(decodeTo.self, from: data)
//                completion(.success(decodedData))
//
//            } catch {
//                print (error)
//                completion(.failure(.decodingError))
//            }
//        }.resume()
//    }
}
